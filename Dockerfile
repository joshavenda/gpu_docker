# We must first define the base image for our model container
#   For GPU training, you need to use an nvidia container
#   DO NOT attempt to install cuda drivers in a container by yourself. This is a big waste of time

#FROM nvidia/cuda:11.0-base-ubuntu18.04

#   If only CPU is required / available:

FROM ubuntu:18.04

#   Cloud providers also provide pre-built images that can be used and extended:

#   For AWS:
#FROM 763104351884.dkr.ecr.us-east-1.amazonaws.com/pytorch-training:1.6.0-gpu-py36-cu101-ubuntu16.04

#   For GCP:
#FROM gcr.io/deeplearning-platform-release/pytorch-gpu.1-6

MAINTAINER Joshua Shubert <josh@avendahealth.com>

# Set some environment variables.
# PYTHONUNBUFFERED keeps Python from buffering our standard output stream,
# which means that logs can be delivered to the user quickly.
# PYTHONDONTWRITEBYTECODE keeps Python from writing the .pyc files which are unnecessary in this case.
ENV PYTHONUNBUFFERED=TRUE
ENV PYTHONDONTWRITEBYTECODE=TRUE

# The use of ARG allows for named arguments to passed in to the docker build process
ARG MODEL_DIR

# Move model files onto container
# ADD $MODEL_DIR /opt/model/
# General dependency + tools installation
ENV TZ=US/Pacific
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update
RUN apt-get -y install --no-install-recommends \
    build-essential \
    ca-certificates \
    python3.7 \
    python3.7-dev \
    curl \
    python3-setuptools \
    && rm -rf /var/lib/apt/lists/* \
    && curl -O https://bootstrap.pypa.io/get-pip.py \
    && python3.7 get-pip.py

# Install gpu_docker package
RUN mkdir /gpu_docker_dist
ADD . /gpu_docker_dist/
RUN cd /gpu_docker_dist/ && python3.7 setup.py install

# Prepare entrypoint script
COPY scripts/router.py /usr/local/bin/
RUN chmod +x /usr/local/bin/router.py
ENTRYPOINT ["python3.7", "/usr/local/bin/router.py"]

# Define command to be passed to the entrypoint
CMD ["train"]
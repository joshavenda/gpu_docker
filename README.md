# gpu_docker manual

`gpu_docker` is a minimal example of how to create a containerized pytorch service packaged and deployed

The code should be tailored to your specific cloud provider and with training loops tailored to your model

## How to build your container

First ensure you have `docker` installed.

See: https://docs.docker.com/get-docker/ for more details on how to install docker on your machine.

After docker is installed, it is recommended to run `build_container.sh` but otherwise run `docker build -t "gpu_docker:latest .`

## How to test your built container locally

Run `sudo docker run gpu_docker:latest` to spawn the service locally

If you wish to run your container locally with GPU support, you must first install the nvidia docker runtime.

See: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html

Once installed, Run `sudo docker run --runtime=nvidia gpu_docker:latest` to spawn the service locally with GPU support

## How to debug your built container locally

Run `sudo docker run -it --entrypoint /bin/bash gpu_docker:latest` to start an interactive
bash shell that you can use to poke around your container and ensure everything is configured properly

## How to share a local directory with the docker image

`sudo docker run -v ~/my_dataset:/opt/data gpu_docker:latest`
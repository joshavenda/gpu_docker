#!/usr/bin/env bash

set -e

CONTAINER_NAME=gpu_docker:latest
MODEL_DIR=some_dir

build_docker_image() {

    docker build \
      --build-arg MODEL_DIR="${MODEL_DIR}" \
      -t "${CONTAINER_NAME}" .
}

deploy_built_container_to_cloud() {
    echo "Deploying to docker container to the cloud!"
#    Vendor dependent
#    For AWS:
#    $(aws ecr get-login --region "${AWS_REGION}" --no-include-email)
#    docker tag "${CONTAINER_NAME}" "${REPOSITORY_URI}"/"${CONTAINER_NAME}"
#    docker push "${REPOSITORY_URI}"/"${CONTAINER_NAME}"
#    GCP is a little different.
#    See for more details: https://cloud.google.com/container-registry/docs/pushing-and-pulling
#    In short you run "gcloud auth configure-docker" and then docker will ask gcloud for authorization
#    when you run the "docker tag" and "docker push" commands

}

main() {
    build_docker_image
    deploy_built_container_to_cloud
}

main "$@"
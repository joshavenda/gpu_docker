import logging
import torch
import random
import numpy as np
from ..model import SimpleCNN
# import boto3
# from google.cloud import storage


def train(epochs=100):

    if torch.cuda.is_available():
        logging.info("CUDA is available. Enabling GPU support")

    model = init_model()

    best_avg_val_loss = np.Inf
    for e in range(epochs):

        avg_train_loss = train_loop(model)
        avg_val_loss = validation_loop(model)

        logging.info(f"Epoch: {e} train_loss: {avg_train_loss} val_loss: {avg_val_loss}")

        if avg_val_loss < best_avg_val_loss:

            best_avg_val_loss = avg_val_loss

            logging.info(f"Epoch: {e} got new best val loss: {avg_val_loss}")
            logging.info("Saving weights to data bucket...")

            upload_artifact_to_cloud()

    logging.info("Training complete")


def train_loop(model: SimpleCNN):
    model.train()
    # for idx, (X, y) in train_loader:
    #     y_hat = model(X)
    #     loss = loss_func(y_hat, y)
    #     optimizer.zero_grad()
    #     loss.backward()
    #     optimizer.step()
    return random.randint(0, 100)


def validation_loop(model: SimpleCNN):
    model.eval()
    # for idx, (X, y) in validation_loader:
    #     y_hat = model(X)
    #     loss = loss_func(y_hat, y)
    return random.randint(0, 100)


def init_model() -> SimpleCNN:
    model = SimpleCNN()

    logging.info(model)

    if torch.cuda.is_available():
        model.cuda()

    return model


def upload_artifact_to_cloud(model_weights_path="", provider='aws'):

    if provider == 'aws':
        logging.info(f"Uploading artifact {model_weights_path} to AWS")
        # s3_client = boto3.client('s3')
        # s3_client.upload_file(
        #   model_weights_path,
        #   'model-artifact-bucket',
        #   model_weights_path
        # )

    elif provider == 'gcp':
        logging.info(f"Uploading artifact {model_weights_path} to GCP")
        # client = storage.Client( project='model-artifact-bucket')
        # bucket = client.get_bucket('model-artifact-bucket')
        # blob = bucket.blob(model_weights_path)
        # blob.upload_from_filename(model_weights_path)

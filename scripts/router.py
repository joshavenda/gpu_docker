import logging
import sys
from gpu_docker.train import train


logging.basicConfig(level=logging.INFO)


def main(router_cmd: str):
    logging.info(f"Starting functionality for command: {router_cmd}")
    if router_cmd == 'train':
        train()


if __name__ == '__main__':
    logging.info("Starting gpu_docker service!")

    if len(sys.argv) < 2:
        logging.error("No router command specified!")

    main(sys.argv[1])
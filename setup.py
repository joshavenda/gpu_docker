import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gpu_docker",
    version="1.0.0",
    author="Joshua Shubert",
    author_email="josh@avendahealth.com",
    description="Repository explaining the basics of using GPU docker containers for ML model training",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/joshavenda/gpu_docker",
    packages=setuptools.find_packages(),
    install_requires=[
        "numpy==1.19.0",
        "scipy==1.5.2",
        "torch==1.6.0",
        "google-cloud-storage==1.32.0",
        "boto3==1.16.17"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    python_requires='>=3.7',
)